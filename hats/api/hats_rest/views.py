from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "import_href",
        "closet_name",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style",
        "fabric",
        "color",
        "picture_url",
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style",
        "fabric",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        # hats = Hat.objects.all()
        # manually iterate over hats to ensure location shows up using "select_related"
        hats = Hat.objects.select_related('location').all()
        hat_list = []
        for hat in hats:
            hat_data = {
                "id": hat.id,
                "style": hat.style,
                "fabric": hat.fabric,
                "color": hat.color,
                "picture_url": hat.picture_url,
                "location": {
                    "id": hat.location.id,
                    "import_href": hat.location.import_href,
                    "closet_name": hat.location.closet_name,
                }
            }
            hat_list.append(hat_data)

        return JsonResponse(
            {"hats": hat_list},
            encoder=HatListEncoder,
        )

    else:
        # "POST"
        content = json.loads(request.body)
        # this gets the LocationVO through "location_href" by using '/api/locations/{location_vo_id}/'
        try:
            location_href = f'/api/locations/{content["location"]}/'
            location_vo = LocationVO.objects.get(import_href=location_href)
            content["location"] = location_vo
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse({"message": "Created"}, status=201)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

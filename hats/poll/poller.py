import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO

def poll():
    while True:
        print("Hats poller polling for data")
        try:

            response = requests.get("http://wardrobe-api:8000/api/locations/")
            if response.status_code == 200:
                locations = response.json()["locations"]
                for location in locations:
                    LocationVO.objects.update_or_create(
                        import_href=location["href"],
                        closet_name=location["closet_name"],
                    )

            else:
                print(f"Failed to fetch locations: Status code {response.status_code}")

        except Exception as e:
            print(f"An error occurred: {e}", file=sys.stderr)
        time.sleep(30)


if __name__ == "__main__":
    poll()

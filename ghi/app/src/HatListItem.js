import React from 'react';
import './index.css';
import { NavLink } from 'react-router-dom'


const HatListItem = function({ hat, deleteHat }) {
    return (
        <div className="1-25 m-3 card-size">
            <img src={hat.picture_url} className="card-img-top img-fluid" alt="Hat" />
            <div className="card-body">
                <h6 className="card-title">{hat.fabric} - {hat.style}</h6>
                <div>Color: {hat.color}</div>
                <div>Location: {hat.location.closet_name}</div>
                <NavLink to={`/hats/${hat.id}`} className="nav-button">Details</NavLink>
                <button onClick={() => deleteHat(hat.id)} className="btn btn-lg btn-danger w-100 mt-3">Delete</button>
            </div>
        </div>
    );
}

export default HatListItem;
